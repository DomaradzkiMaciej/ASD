#include <iostream>
#include <vector>

using namespace std;

int main() {
  ios_base::sync_with_stdio(false);
  cin.tie(nullptr);

  long long n, a, cnt = 0, x, y;
  vector<long long> seen(11, 0);

  cin >> n;

  for (long long i = 0; i < n; ++i) {
    cin >> a;

    for (long long j = 1; j <= a/2; ++j) {
      x = seen.at(j);
      y = seen.at(a - j);

      if (j == a-j)
        cnt += x*(x-1)/2;
      else
        cnt += x*y;
    }

    seen.at(a) += 1;
  }

  cout << cnt;
}
