#include <iostream>
#include <cmath>
#include <tuple>
#include <algorithm>

using namespace std;

const int SIZE = 524288;

int n;
int m;

char word[SIZE];
int hash_table[SIZE][20];
tuple<int, int, int> my_tuple[SIZE];

void count_hash() {
    int lg = (int) log2(n);

    for (int i = 0; i < n; ++i) {
        hash_table[i][0] = (int) word[i];
    }

    for (int i = 1; i <= lg; ++i) {
        for (int j = 0; j < n - (1<<i) + 1; ++j) {
            my_tuple[j] = tuple<int, int, int>(hash_table[j][i - 1], hash_table[j + (1 << (i - 1))][i - 1], j);
        }

        sort(my_tuple, my_tuple + n - (1<<i) + 1);

        for (int j = 0, k = 1; j < n - (1<<i) + 1; ++j) {
            if (j != 0 && (get<0>(my_tuple[j - 1]) != get<0>(my_tuple[j]) ||
                get<1>(my_tuple[j - 1]) != get<1>(my_tuple[j])))
                ++k;

            hash_table[get<2>(my_tuple[j])][i] = k;
        }
    }
}

char count(int b1, int e1, int b2, int e2) {
    b1 -=1;
    e1 -=1;
    b2 -= 1;
    e2 -=1;

    int length = min(e1 - b1 + 1, e2 - b2 + 1);
    int lg = (int) log2(length);
    int s = (1 << lg);

    if (hash_table[b1][lg] < hash_table[b2][lg])
        return '<';

    if (hash_table[b1][lg] > hash_table[b2][lg])
        return '>';

    if (hash_table[b1 + length - s][lg] < hash_table[b2 + length- s][lg])
        return '<';

    if (hash_table[b1 + length - s][lg] > hash_table[b2 + length - s][lg])
        return '>';

    if (e1 - b1 < e2 - b2)
        return '<';

    if (e1 - b1 > e2 - b2)
        return '>';

    return '=';
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    cin >> n >> m;

    for (auto[i, c] = tuple{0, 'a'}; i < n; ++i) {
        cin >> c;
        word[i] = c;
    }

    count_hash();
    for (int i = 0, b1, e1, b2, e2; i < m; ++i) {
        cin >> b1 >> e1 >> b2 >> e2;
        cout << count(b1, e1, b2, e2) << "\n";
    }

    return 0;
}