#include <iostream>
#include <vector>

const int DIVISOR = 1000000000;

int main() {
    int n;
    int d;
    int first;
    int second;

    std::cin >> n;
    std::cin >> d;

    std::vector<std::pair<int, int>> nodes;
    std::vector<std::pair<int, int>> nodes_sum(n+1, std::pair<int ,int>());

    nodes.emplace_back(0,0); //wezly numerujemy od 1, wiec zerowy element nie jest wazny
    nodes_sum.at(0).first = 1;
    nodes_sum.at(0).second = 0;

    for (int i = 0; i < n; ++i) {
        std::cin >> first;
        std::cin >> second;
        nodes.emplace_back(first,second);
    }

    for (int i = 1; i <= n ; ++i) {
        std::pair<int, int> node = nodes.at(i);
        nodes_sum.at(i).first = nodes_sum.at(node.first).first % DIVISOR + nodes_sum.at(node.second).first % DIVISOR+ 1;
    }

    for (int i = 1; i <= d; ++i) {
        for (int j = 1; j <= n; ++j) {
            std::pair<int, int> node = nodes.at(j);
            nodes_sum.at(j).second = nodes_sum.at(node.first).first % DIVISOR + nodes_sum.at(node.second).first % DIVISOR;
        }

        for (int j = 0; j <= n; ++j) {
            nodes_sum.at(j).first = nodes_sum.at(j).second;
        }
    }

    std::cout << nodes_sum.at(n).first % DIVISOR;

    return 0;
}