#include <cstdio>

#define SIZE 3000000

int tree[SIZE] = {0};
int lazy[SIZE] = {};
int n;

bool initialize() {
    for (auto &element : lazy)
        element = -1;

    return true;
}

void updateRange(int currentNode, int rangeFirst, int rangeLast, int first, int last, int color) {
    if (rangeFirst > last || rangeLast < first)
        return;

    if (lazy[currentNode] != -1) {
        tree[currentNode] = (rangeLast - rangeFirst + 1) * lazy[currentNode];

        if (rangeFirst != rangeLast) {
            lazy[2 * currentNode] = lazy[currentNode];
            lazy[2 * currentNode + 1] = lazy[currentNode];

            int mid = (rangeFirst + rangeLast) / 2;
            tree[2 * currentNode] = (mid - rangeFirst + 1) * lazy[currentNode];
            tree[2 * currentNode + 1] = (rangeLast - mid) * lazy[currentNode];
        }

        lazy[currentNode] = -1;
    }

    if (rangeFirst > rangeLast || rangeFirst > last || rangeLast < first)
        return;

    if (rangeFirst >= first && rangeLast <= last) {
        tree[currentNode] = (rangeLast - rangeFirst + 1) * color;
        lazy[currentNode] = color;

        return;
    }

    int mid = (rangeFirst + rangeLast) / 2;
    updateRange(currentNode * 2, rangeFirst, mid, first, last, color);
    updateRange(currentNode * 2 + 1, mid + 1, rangeLast, first, last, color);

    tree[currentNode] = tree[currentNode * 2] + tree[currentNode * 2 + 1];
}

void update(int first, int last, int color) {
    updateRange(1, 1, n, first, last, color);
}

bool x = initialize();

int main() {
    int m, a, b;
    char color;

    scanf("%i %i", &n, &m);

    for (int i = 0; i < m; ++i) {
        scanf("%i %i %c", &a, &b, &color);

        if (color == 'B')
            update(a, b, 1);
        else
            update(a, b, 0);

        printf("%i \n", tree[1]);
    }

    return 0;
}