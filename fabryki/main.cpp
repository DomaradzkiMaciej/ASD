#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

struct node {
    long long type;

    vector<long long> deep;
    vector<long long> length;

    vector<long long> neighbours;

    node() : type(0), deep(vector<long long>(11, -1)), length(vector<long long>(11, 0)), neighbours() {}
};

void count(long long position, long long father, vector<node> &nodes) {
    auto &myNode = nodes.at(position);
    long long d;

    for (auto neighbour : myNode.neighbours) {
        if (neighbour == father)
            continue;

        count(neighbour, position, nodes);
    }

    for (int i = 1; i <= 10; ++i) {
        d = 0;

        for (auto neighbour : myNode.neighbours) {
            if (neighbour == father)
                continue;

            auto &son = nodes.at(neighbour);

            if (son.deep.at(i) != -1) {
                if (myNode.deep.at(i) < son.deep.at(i) + 1) {
                    if (d < myNode.deep.at(i))
                        d = myNode.deep.at(i);
                    myNode.deep.at(i) = son.deep.at(i) + 1;
                } else if (d < son.deep.at(i) + 1) {
                    d = son.deep.at(i) + 1;
                }
            }

            if (myNode.length.at(i) < son.length.at(i))
                myNode.length.at(i) = son.length.at(i);

        }

        if (d != 0 || myNode.type == i)
            myNode.length.at(i) = max(myNode.length.at(i), myNode.deep.at(i) + d);
    }
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    long long n, type, x, y;
    vector<node> nodes(200001);

    cin >> n;

    for (long long i = 1; i <= n; ++i) {
        cin >> type;

        nodes.at(i).type = type;
        nodes.at(i).deep.at(type) = 0;
    }

    for (long long i = 1; i <= n - 1; ++i) {
        cin >> x >> y;

        nodes.at(x).neighbours.push_back(y);
        nodes.at(y).neighbours.push_back(x);
    }

    count(1, -1, nodes);

    long long max = 0;

    for (auto len : nodes.at(1).length) {
        if (max < len)
            max = len;
    }

    cout << max;

    return 0;
}
