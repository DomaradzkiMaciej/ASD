#include <iostream>
#include <algorithm>

using namespace std;

unsigned long findLength (const string& pattern) {
    unsigned long length = pattern.size() - 1;
    unsigned long current_length = 0;
    char color;

    unsigned long i;
    for (i=0; i < pattern.size(); i++)
        if (pattern.at(i) != '*'){
            color = pattern.at(i);
            break;
        }

    if (i == pattern.size())
        return 1;

    for (; i < pattern.size(); ++i) {
        if (pattern.at(i) == '*'){
            current_length++;
            continue;
        }

        if (pattern.at(i) == color){
            current_length = 0;
            continue;
        }

        length = min(current_length, length);
        current_length = 0;
        color = pattern.at(i);
    }

    return pattern.size() - length;
}

int main() {
    string pattern;
    unsigned long length;

    cin >> pattern;
    length = findLength(pattern);
    cout << length << endl;

    return 0;
}