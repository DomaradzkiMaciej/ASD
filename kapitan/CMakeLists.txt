cmake_minimum_required(VERSION 3.15)
project(kapitan)

set(CMAKE_CXX_STANDARD 17)

add_executable(kapitan main.cpp)