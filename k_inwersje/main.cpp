#include <iostream>
#include <vector>

#define BIG 1000000000

int modulo(int value){
    while (value < 0)
        value += BIG;

    return  value % BIG;
}

struct interval_tree {
    int extraElementsNr;
    std::vector<int> tree;

    void add(int position, int value) {
        int place = position + extraElementsNr;
        tree[place] = value;

        while (place != 1) {
            place /= 2;
            tree[place] = modulo(tree[2 * place] + tree[2 * place + 1]);
        }
    }

    int count(int first, int last) {
        return modulo(count_from_left(last) - count_from_left(first - 1));
    }

    int get(int position) {
        return tree[position + extraElementsNr];
    }

private:
    int count_from_left(int last) {
        if (last < 0)
            return 0;

        int position = extraElementsNr + last;
        int sum = tree[position];

        while (position != 1) {
            if (position % 2 == 1)
                sum = modulo(sum + tree[position - 1]);

            position /= 2;
        }

        return sum;
    }

};

int first_bigger_multiple_of_2(int number) {
    int prime = 1;

    while (prime < number)
        prime *= 2;

    return prime;
}

std::vector<int> reverse_elements(std::vector<int> &elements) {
    std::vector<int> reverseElements(elements.size(), 0);

    for (size_t i = 0; i < elements.size(); ++i) {
        reverseElements[elements[i]] = i;
    }

    return reverseElements;
}

int get_answer(std::vector<int> &reverseElements, int k, int n) {

    int prime = first_bigger_multiple_of_2(n);
    int sum = 0;
    std::vector<interval_tree> treeList(k, {prime, std::vector<int >(2 * prime, 0)});

    for (auto element : reverseElements) {
        treeList[0].add(element, 1);

        for (int j = 1; j < k; ++j) {
            treeList[j].add(element, treeList[j - 1].count(element + 1, n - 1));
        }
    }

    for (int i = 0; i < n; ++i) {
        sum = modulo(sum + treeList[k - 1].get(i));
    }

    return (modulo(sum));
}

int main() {
    int n;
    int k;

    std::vector<int> elements;
    std::vector<int> reverseElements;

    std::cin >> n;
    std::cin >> k;

    for (int i = 0, element; i < n; ++i) {
        std::cin >> element;
        elements.push_back(element - 1);
    }

    reverseElements = reverse_elements(elements);

    std::cout << get_answer(reverseElements, k, n);

    return 0;
}