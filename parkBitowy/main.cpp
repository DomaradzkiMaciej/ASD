#include <iostream>
#include <vector>
#include <ctgmath>
#include <cassert>

int n;
int m;
int logFloor;

int left[500001];
int right[500001];
int parent[500001];
int depth[500001];
int links[20][500001];

std::pair<int, int> farDown[500001];
std::pair<int, int> farUp[500001];
std::pair<int, int> far[500001];

int ancestor(int v, int h) {
    int res = v;
    int i = logFloor;

    while (h > 0) {
        if ((1 << i) > h)
            i -= 1;
        else {
            res = links[i][res];
            h -= (int) (1 << i);
        }
    }

    return res;
}

int lca(int u, int v) {
    int du = depth[u];
    int dv = depth[v];

    if (du < dv) {
        v = ancestor(v, dv - du);
        dv = depth[v];
    } else if (du > dv) {
        u = ancestor(u, du - dv);
        du = depth[u];
    }
    assert(du == dv);

    if (u == v)
        return u;

    int i = logFloor;

    while (i >= 0) {
        if (links[i][u] != links[i][v]) {
            u = links[i][u];
            v = links[i][v];
        }

        i -= 1;
    }

    return parent[u];
}

void fillFarDown(int i) {
    if (left[i] != -1)
        fillFarDown(left[i]);
    if (right[i] != -1)
        fillFarDown(right[i]);

    if (left[i] == -1 && right[i] == -1) {
        farDown[i].first = 0;
        farDown[i].second = i;
    } else if (left[i] == -1) {
        farDown[i].first = farDown[right[i]].first + 1;
        farDown[i].second = farDown[right[i]].second;
    } else if (right[i] == -1) {
        farDown[i].first = farDown[left[i]].first + 1;
        farDown[i].second = farDown[left[i]].second;
    } else {
        if (farDown[left[i]].first > farDown[right[i]].first) {
            farDown[i].first = farDown[left[i]].first + 1;
            farDown[i].second = farDown[left[i]].second;
        } else {
            farDown[i].first = farDown[right[i]].first + 1;
            farDown[i].second = farDown[right[i]].second;
        }
    }
}

void fillFarUp(int i) {
    int sibling = 0;
    if (i != 1) {
        if (right[parent[i]] == i)
            sibling = left[parent[i]];
        else
            sibling = right[parent[i]];
    }
    if (i == 1) {
        farUp[i].first = 0;
        farUp[i].second = i;
    } else if (sibling == -1) {
        farUp[i].first = farUp[parent[i]].first + 1;
        farUp[i].second = farUp[parent[i]].second;
    } else {
        if (farUp[parent[i]].first + 1 > farDown[sibling].first + 2) {
            farUp[i].first = farUp[parent[i]].first + 1;
            farUp[i].second = farUp[parent[i]].second;
        } else {
            farUp[i].first = farDown[sibling].first + 2;
            farUp[i].second = farDown[sibling].second;
        }
    }

    if (left[i] != -1)
        fillFarUp(left[i]);

    if (right[i] != -1)
        fillFarUp(right[i]);
}

void fillFar() {
    fillFarDown(1);
    fillFarUp(1);

    for (int i = 1; i <= n; ++i) {
        if (farDown[i].first > farUp[i].first) {
            far[i].first = farDown[i].first;
            far[i].second = farDown[i].second;
        } else {
            far[i].first = farUp[i].first;
            far[i].second = farUp[i].second;
        }
    }
}

void fillDepth(int i) {
    if (i == 1)
        depth[i] = 0;
    else
        depth[i] = depth[parent[i]] + 1;

    if (left[i] != -1)
        fillDepth(left[i]);
    if (right[i] != -1)
        fillDepth(right[i]);
}

int main() {
    std::ios_base::sync_with_stdio(false);
    std::cin.tie(nullptr);

    std::cin >> n;

    logFloor = floor(log(n));

    for (int i = 1, k, l; i <= n; ++i) {
        std::cin >> k;
        std::cin >> l;

        left[i] = k;
        right[i] = l;

        if (k != -1)
            parent[k] = i;
        if (l != -1)
            parent[l] = i;
    }

    fillDepth(1);

    for (int v = 1; v <= n; ++v) {
        links[0][v] = parent[v];
    }

    for (int i = 1; i <= logFloor; ++i) {
        for (int v = 1; v <= n; ++v) {
            if (links[i - 1][v] != -1)
                links[i][v] = links[i - 1][links[i - 1][v]];
            else
                links[i][v] = -1;
        }
    }

    fillFar();
    std::cin >> m;

    for (int i = 0, u, d; i < m; ++i) {
        std::cin >> u;
        std::cin >> d;

        auto &[d_max, v] = far[u];

        if (d > d_max) {
            std::cout << -1 << "\n";
            continue;
        }

        int l = lca(u, v);
        int d1 = depth[u] - depth[l];

        if (d <= d1)
            std::cout << ancestor(u, d) << "\n";
        else
            std::cout << ancestor(v, d_max - d) << "\n";
    }
}