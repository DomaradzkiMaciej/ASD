#include <algorithm>
#include <iostream>
#include <tuple>
// https://cp-algorithms.com/data_structures/treap.html
using namespace std;

const int DIVISOR = 1000000007;
using pnode = struct node *;

int priorities[200000];
bool is_roman[200000];
int my_array[200000];

long long mod(long long val) {
    val = val % DIVISOR;
    return (val < 0) ? val + DIVISOR : val;
}

bool initialize() {
    for (int &priority : priorities) {
        priority = rand();
    }

    return true;
}

bool _ = initialize();
int in_position = 0;

struct node {
    int priority;
    int position;
    bool is_roman;
    long long cnt;

    long long sseq;
    long long sseq_end_0;
    long long sseq_begin_0;
    long long sseq_both_sides_0;

    bool rev;
    pnode l, r;

    node(int priority, int position, bool is_roman) : priority(priority), is_roman(is_roman), position(position),
                                                      cnt(0),
                                                      sseq(0), sseq_end_0(0), sseq_begin_0(0), sseq_both_sides_0(0),
                                                      rev(false), l(nullptr), r(nullptr) {}
};

long long cnt(pnode root) {
    return (root != nullptr) ? root->cnt : 0;
}

long long sseq(pnode root) {
    return (root != nullptr) ? root->sseq : 0;
}

long long sseq_end_0(pnode root, pnode parent) {
    if (root == nullptr)
        return 0;
    else
        return (root->rev != parent->rev) ? root->sseq_begin_0 : root->sseq_end_0;
}

long long sseq_begin_0(pnode root, pnode parent) {
    if (root == nullptr)
        return 0;
    else
        return (root->rev != parent->rev) ? root->sseq_end_0 : root->sseq_begin_0;
}

long long sseq_both_sides_0(pnode root) {
    return (root != nullptr) ? root->sseq_both_sides_0 : 0;
}

void upd(pnode root) {
    long long my_sseq;
    long long my_sseq_end_0;
    long long my_sseq_begin_0;
    long long my_sseq_both_sides_0;

    if (root != nullptr) {
        root->cnt = cnt(root->l) + cnt(root->r) + 1;

        if (root->is_roman) {
            my_sseq = mod(sseq(root->l) * 2 + 1);
            my_sseq_end_0 = mod(sseq_end_0(root->l, root));
            my_sseq_begin_0 = mod(sseq_begin_0(root->l, root) * 2);
            my_sseq_both_sides_0 = mod(sseq_both_sides_0(root->l));
        } else {
            my_sseq = mod(sseq(root->l) * 2 + 1 - sseq_end_0(root->l, root));
            my_sseq_end_0 = mod(sseq(root->l) + 1);
            my_sseq_begin_0 = mod(sseq_begin_0(root->l, root) * 2 + 1 - sseq_both_sides_0(root->l));
            my_sseq_both_sides_0 = mod(sseq_begin_0(root->l, root) + 1);
        }

        root->sseq = mod((my_sseq + 1) * (sseq(root->r) + 1) - 1 - my_sseq_end_0 * sseq_begin_0(root->r, root));
        root->sseq_end_0 =
                mod((my_sseq + 1) * sseq_end_0(root->r, root) - my_sseq_end_0 * sseq_both_sides_0(root->r) +
                my_sseq_end_0);
        root->sseq_begin_0 =
                mod(my_sseq_begin_0 * (sseq(root->r) + 1) - my_sseq_both_sides_0 * sseq_begin_0(root->r, root) +
                sseq_begin_0(root->r, root));
        root->sseq_both_sides_0 =
                mod(my_sseq_begin_0 * sseq_end_0(root->r, root) - my_sseq_both_sides_0 * sseq_both_sides_0(root->r) +
                my_sseq_both_sides_0 + sseq_both_sides_0(root->r));
    }
}

void repair_priority(pnode root) {
    if (root == nullptr)
        return;

    pnode max = root;

    if (root->l != nullptr && root->l->priority > max->priority)
        max = root->l;

    if (root->r != nullptr && root->r->priority > max->priority)
        max = root->r;

    if (max != root) {
        swap(root->priority, max->priority);
        repair_priority(max);
    }
}

pnode build(int *array, bool *positions, int size) {
    if (size == 0)
        return nullptr;

    int mid = size / 2;
    auto root = new node(priorities[in_position++], array[mid], positions[mid]);

    root->l = build(array, positions, mid);
    root->r = build(array + mid + 1, positions + mid + 1, size - mid - 1);

    repair_priority(root);
    upd(root);

    return root;
}

void rev(pnode root) {
    if (root != nullptr && root->rev) {
        root->rev = false;
        swap(root->l, root->r);
        swap(root->sseq_begin_0, root->sseq_end_0);

        if (root->l)
            root->l->rev ^= true;

        if (root->r)
            root->r->rev ^= true;
    }
}

void merge(pnode &root, pnode l, pnode r) {
    rev(l);
    rev(r);

    if (l == nullptr || r == nullptr) {
        root = (l != nullptr) ? l : r;
    } else if (l->priority > r->priority) {
        root = l;
        merge(l->r, l->r, r);
    } else {
        root = r;
        merge(r->l, l, r->l);
    }

    upd(root);
}

void split(pnode root, pnode &l, pnode &r, int key, long long add = 0) {
    if (root == nullptr) {
        l = nullptr;
        r = nullptr;

        return;
    }

    rev(root);

    if (key <= add + cnt(root->l)) {
        r = root;
        split(root->l, l, root->l, key, add);
    } else {
        l = root;
        split(root->r, root->r, r, key, add + 1 + cnt(root->l));
    }

    upd(root);
}

void reverse(pnode root, int l, int r) {
    l -= 1;
    r -= 1;

    pnode t1, t2, t3;

    split(root, t1, t2, l);
    split(t2, t2, t3, r - l + 1);

    t2->rev ^= true;

    merge(root, t1, t2);
    merge(root, root, t3);
}

void choices_nr(pnode root, int l, int r) {
    l -= 1;
    r -= 1;

    pnode root1, root2, root3;

    split(root, root1, root2, l);
    split(root2, root2, root3, r - l + 1);

    cout << root2->sseq << "\n";

    merge(root, root1, root2);
    merge(root, root, root3);
}

int main() {
    int n, m;
    cin >> n >> m;

    for (auto[i, c] = pair{0, 'a'}; i < n; ++i) {
        cin >> c;

        if (c == 'R')
            is_roman[i] = true;
        else
            is_roman[i] = false;

        my_array[i] = i;
    }

    pnode t = build(my_array, is_roman, n);

    for (auto[i, c, x, y] = tuple{0, 'a', 0, 0}; i < m; ++i) {
        cin >> c >> x >> y;

        if (c == '?')
            choices_nr(t, x, y);
        else
            reverse(t, x, y);
    }
}
