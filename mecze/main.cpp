#include <iostream>
#include <vector>
#include <algorithm>

std::vector<long long> read(){
    int playersAmount;
    int gamesAmount;
    int playerNr;

    std::string line;

    std::cin >> playersAmount;
    std::cin >> gamesAmount;

    std::vector<long long> players(playersAmount, 0);

    for (int i = 0; i < gamesAmount; ++i) {
        for (auto &player : players)
            player *= 2;

        for (int j = 0; j < playersAmount/2; ++j) {
            std::cin >> playerNr;
            players[playerNr - 1] +=1;
        }

        getline(std::cin, line);

    }
    return players;
}

int main() {
    auto players = read();
    std::sort(players.begin(), players.end());

    for (size_t i = 0; i < players.size() - 1; ++i)
        if (players[i] == players[i+1]){
            std::cout << "NIE";
            return 0;
        }

    std::cout << "TAK";

    return 0;
}