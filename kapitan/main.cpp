#include <iostream>
#include <algorithm>
#include <climits>
#include <queue>

using namespace std;

struct island {
public:
    int length;
    int position;
    int NS;
    int WE;

    island() : length(0), position(1), NS(0), WE(0) {}

    island(int length, int position, int NS, int WE) :
            length(length), position(position), NS(NS), WE(WE) {}

    friend bool operator>(const island &lhs, const island &rhs) {
        return lhs.length > rhs.length;
    }
};

struct trio {
    int NS;
    int WE;
    int position;
};

const int MAX_SIZE = 300000;

int n;
trio coordinatesNS[MAX_SIZE];
trio coordinatesWE[MAX_SIZE];
pair<int, int> coordinates[MAX_SIZE];

int length[MAX_SIZE];
bool visited[MAX_SIZE];

priority_queue<island, vector<island>, greater<>> pq;
island my_island;

bool initialize() {
    for (int i = 2; i < MAX_SIZE; ++i) {
        length[i] = INT_MAX;
    }

    return true;
}

bool _ = initialize();

int my_min(trio &a, trio &b) {
    if (b.position == 0)
        return -1;

    return min(abs(a.NS - b.NS), abs(a.WE - b.WE));
}

void try_add_neighbour(int l, trio &actual, trio &next) {
    int len = l + my_min(actual, next);

    if (len == -1 + l)
        return;

    if (len < length[next.position]) {
        length[next.position] = len;
        pq.push(island(len, next.position, coordinates[next.position].first, coordinates[next.position].second));
    }
}

void add_neighbours() {
    try_add_neighbour(my_island.length, coordinatesNS[my_island.NS], coordinatesNS[my_island.NS + 1]);
    try_add_neighbour(my_island.length, coordinatesNS[my_island.NS], coordinatesNS[my_island.NS - 1]);
    try_add_neighbour(my_island.length, coordinatesWE[my_island.WE], coordinatesWE[my_island.WE + 1]);
    try_add_neighbour(my_island.length, coordinatesWE[my_island.WE], coordinatesWE[my_island.WE - 1]);
}

void add_beginning() {
    int x = -1;
    int y = -1;

    for (int i = 1; i <= n; ++i) {
        if (coordinatesNS[i].position == 1)
            x = i;
    }

    for (int i = 1; i <= n; ++i) {
        if (coordinatesWE[i].position == 1)
            y = i;
    }

    my_island.NS = x;
    my_island.WE = y;

    visited[1] = true;
    add_neighbours();
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    cin >> n;
    for (int i = 1, x, y; i <= n; ++i) {
        cin >> x;
        cin >> y;

        coordinatesNS[i].NS = x;
        coordinatesNS[i].WE = y;
        coordinatesNS[i].position = i;

        coordinatesWE[i].NS = x;
        coordinatesWE[i].WE = y;
        coordinatesWE[i].position = i;
    }

    sort(coordinatesNS + 1, coordinatesNS + n + 1, [](trio a, trio b) {
        return a.NS < b.NS || (a.NS == b.NS && a.WE < b.WE);
    });
    sort(coordinatesWE + 1, coordinatesWE + n + 1, [](trio a, trio b) {
        return a.WE < b.WE || (a.WE == b.WE && a.NS < b.NS);
    });

    for (int i = 1; i <= n; ++i) {
        coordinates[coordinatesNS[i].position].first = i;
    }

    for (int i = 1; i <= n; ++i) {
        coordinates[coordinatesWE[i].position].second = i;
    }

    add_beginning();

    while (!pq.empty()) {
        my_island = pq.top();
        pq.pop();

        if (visited[my_island.position])
            continue;

        visited[my_island.position] = true;
        add_neighbours();
    }

    cout <<length[n];

    return 0;
}
