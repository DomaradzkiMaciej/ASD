#include <iostream>
#include <vector>
#include <queue>
#include <utility>
#include <algorithm>

using namespace std;
using pair_ = pair<int, int>;

vector<int> parent[100001];
vector<int> children[100001];
int value[100001];

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    priority_queue<pair_, vector<pair_>, greater<>> pq;
    int n, m, k, max_ = 0;
    cin >> n >> m >> k;

    for (int i = 1, v; i <= n; ++i) {
        cin >> v;
        value[i] = v;
    }

    for (int i = 1, a, b; i <= m; ++i) {
        cin >> a >> b;
        children[a].emplace_back(b);
        parent[b].emplace_back(a);
    }

    for (int i = 1; i <= n; ++i) {
        if (children[i].empty())
            pq.emplace(value[i], i);
    }

    for (auto[i, top] = pair{k, pair{0, 0}}; i > 0; --i) {
        top = pq.top();
        pq.pop();

        max_ = max(max_, top.first);

        for (auto y : parent[top.second]) {
            auto &vec = children[y];
            vec.erase(remove(vec.begin(), vec.end(), top.second), vec.end());

            if (children[y].empty())
                pq.emplace(value[y], y);
        }
    }

    cout << max_ << endl;
}
