#include <iostream>
#include <algorithm>
#include <vector>

using namespace std;

struct auxiliarySum{
    long long sum;
    int biggestOddUnused;
    int biggestEvenUnused;
    int smallestOddUsed;
    int smallestEvenUnused;
};

void doAuxiliarySums (vector<int> &prices, vector<auxiliarySum> &auxiliary_sums) {
    long long sum = 0;
    int biggestOddUnused = -1;
    int biggestEvenUnused = -1;
    int smallestOddUsed = -1;
    int smallestEvenUnused = -1;

    for (int i = prices.size() - 1; i >= 0; --i) {
        sum += prices.at(i);
        auxiliary_sums.at(i).sum = sum;
    }

    for (int i = prices.size() - 1; i >= 0; --i) {
        if (prices.at(i) % 2 == 0)
            smallestOddUsed = prices.at(i);
        else
            smallestEvenUnused = prices.at(i);

        auxiliary_sums.at(i).smallestOddUsed = smallestOddUsed;
        auxiliary_sums.at(i).smallestEvenUnused = smallestEvenUnused;
    }

    auxiliary_sums.at(0).biggestOddUnused = biggestOddUnused;
    auxiliary_sums.at(0).biggestEvenUnused = biggestEvenUnused;

    for (int i = 0; i < prices.size() - 1; ++i) {
        if (prices.at(i) % 2 == 0)
            biggestOddUnused = prices.at(i);
        else
            biggestEvenUnused = prices.at(i);

        auxiliary_sums.at(i + 1).biggestOddUnused = biggestOddUnused;
        auxiliary_sums.at(i + 1).biggestEvenUnused = biggestEvenUnused;
    }
}
void count (auxiliarySum& auxiliary_sum) {
    if (auxiliary_sum.sum % 2 == 1) {
        cout << auxiliary_sum.sum << endl;
    }

    else if ((auxiliary_sum.biggestEvenUnused == -1 && auxiliary_sum.biggestOddUnused == -1) ||
        (auxiliary_sum.biggestEvenUnused == -1 && auxiliary_sum.smallestEvenUnused == -1) ||
        (auxiliary_sum.smallestEvenUnused == -1 && auxiliary_sum.smallestOddUsed == -1) ||
        (auxiliary_sum.biggestOddUnused == -1 && auxiliary_sum.smallestOddUsed == -1)) {
        cout << -1 << endl;
    }

    else if (auxiliary_sum.biggestOddUnused == -1 || auxiliary_sum.smallestEvenUnused == -1) {
        cout << auxiliary_sum.sum + auxiliary_sum.biggestEvenUnused - auxiliary_sum.smallestOddUsed << endl;
    }

    else if (auxiliary_sum.biggestEvenUnused == -1 || auxiliary_sum.smallestOddUsed == -1) {
        cout << auxiliary_sum.sum + auxiliary_sum.biggestOddUnused - auxiliary_sum.smallestEvenUnused << endl;
    }

    else {
        cout << max(auxiliary_sum.sum + auxiliary_sum.biggestEvenUnused - auxiliary_sum.smallestOddUsed,
                    auxiliary_sum.sum + auxiliary_sum.biggestOddUnused - auxiliary_sum.smallestEvenUnused) << endl;
    }
}


int main() {
    int length;
    int numberOfDays;
    int amount;
    int price;

    cin >> length;

    vector<int> prices;
    vector<auxiliarySum> auxiliary_sums(length);

    for (int i = 0; i < length; ++i) {
        cin >> price;
        prices.push_back( price);
    }

    cin >> numberOfDays;
    doAuxiliarySums(prices, auxiliary_sums);

    while (cin >> amount){
        count (auxiliary_sums.at(auxiliary_sums.size() - amount));
    }

    return 0;
}